# banoffe
                   
                        <li>Tiramisu </li>
                        <li>Chocolate</li>
                        <li>Chapucino</li>


<!DOCTYPE html>

<html>



<head>



    <meta charset="utf-8" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.0/semantic.min.css" integrity="sha256-/mC8AIsSmTcTtaf8vgnfbZXZLYhJCd0b9If/M0Y5nDw=" crossorigin="anonymous" />

    <link rel="stylesheet" href="devices.min.css" />

    <link rel="stylesheet" href="main.css" />

    <link rel="icon" type="image/x-icon" href="favicon.ico">

    <title>Get new iPhone for free!</title>



</head>



<body>



  <div class="marvel-device iphone-x" id="DDDD">

      <div class="notch">

          <div class="camera"></div>

          <div class="speaker"></div>

      </div>

      <div class="top-bar"></div>

      <div class="sleep"></div>

      <div class="bottom-bar"></div>

      <div class="volume"></div>

      <div class="overflow">

          <div class="shadow shadow--tr"></div>

          <div class="shadow shadow--tl"></div>

          <div class="shadow shadow--br"></div>

          <div class="shadow shadow--bl"></div>

      </div>

      <div class="inner-shadow"></div>

      <div class="screen">







          <div class="ui fluid container">



            <div class="ui centered grid">



                <div class="center aligned column apple-header">



                    <img class="ui centered image apple-logo-d" src="logo.png"></div>



            </div>



            <div class="center aligned column step-1">



                <h1 class="ui centered header mt2 mb5">Get iPhone 11 for free!</h1>



                <p class="f14">Select which model would you like to get and click continue.</p>



                <div class="ui top attached tabular menu">

                    <a class="active item" data-tab="65">

                        <p>iPhone 11</p>

                    </a>

                    <a class="item" data-tab="58">

                        <p>iPhone 11 Pro</p>

                    </a>

                    <a class="item" data-tab="61">

                        <p>iPhone 11 <br>Pro Max</p>

                    </a>

                </div>

                <div class="ui buttom attached active tab segment" id="HATEPADDING" data-tab="65">

                    <h2 class="ui centered header mb5">6.1" iPhone 11</h2>

                    <img class="ui centered image iphone" src="11.jpg">

                    <button class="ui big black fluid button mt1 mb0 apple-button" id="HATEPADDING" onclick="step('11');">Continue</button>

                    <ul class="ui list f12 c444">

                        <li>A13 Bionic chip with third-generation Neural Engine</li>

                        <li>Liquid Retina HD display</li>

                        <li>Glass and aluminum design</li>

                        <li>Dual 12MP Ultra Wide and Wide cameras with Night mode</li>

                        <li>Lasts up to 1 hour longer than iPhone XR</li>

                        <li>Water resistant to a depth of 2 meters for up to 30 minutes</li>

                        <li>64GB/128GB/256GB capacity</li>

                        <li>6 available colors</li>

                    </ul>

                </div>

                <div class="ui buttom attached tab segment" id="HATEPADDING" data-tab="58">

                    <h2 class="ui centered header mb5">5.8" iPhone 11 Pro</h2>

                    <img class="ui centered image iphone" src="11PRO.jpg">

                    <button class="ui big black fluid button mt1 mb0 apple-button" id="HATEPADDING" onclick="step('11PRO');">Continue</button>

                    <ul class="ui list f12 c444">

                      <li>A13 Bionic chip with third-generation Neural Engine</li>

                      <li>Super Retina XDR display</li>

                      <li>Textured matte glass and stainless steel design</li>

                      <li>Triple 12MP Ultra Wide, Wide, and Telephoto cameras with Night mode</li>

                      <li>Lasts up to 4 hours longer than iPhone XS</li>

                      <li>Water resistant to a depth of 2 meters for up to 30 minutes</li>

                      <li>64GB/256GB/512GB capacity</li>

                      <li>4 available colors</li>

                    </ul>

                </div>

                <div class="ui buttom attached tab segment" id="HATEPADDING" data-tab="61">

                    <h2 class="ui centered header mb5">6.5" iPhone 11 Pro Max</h2>

                    <img class="ui centered image iphone" src="11PROMAX.jpg">

                    <button class="ui big black fluid button mt1 mb0 apple-button" id="HATEPADDING" onclick="step('11PROMAX');">Continue</button>

                    <ul class="ui list f12 c444">

                      <li>A13 Bionic chip with third-generation Neural Engine</li>

                      <li>Super Retina XDR display</li>

                      <li>Textured matte glass and stainless steel design</li>

                      <li>Triple 12MP Ultra Wide, Wide, and Telephoto cameras with Night mode</li>

                      <li>Longest battery life in an iPhone. Lasts up to 5 hours longer than iPhone XS Max</li>

                      <li>Water resistant to a depth of 2 meters for up to 30 minutes</li>

                      <li>64GB/256GB/512GB capacity</li>

                      <li>4 available colors</li>

                    </ul>

                </div>



            </div>



            <div class="center aligned column step-2" style="display: none;">



              <h2 class="ui centered header mt2 mb0">Choose your iPhone 11 color.</h2>



              <div class="ui centered four column grid ml0 mr0 mt0" id="COLORS6D">



                <div class="center aligned column pb0" style="padding-left: 0 !important; padding-right: 0 !important">



                  <div class="IPCD IP-WHITE" onclick="CCOLOR('WHITE');"></div>

                  White



                </div>



                <div class="center aligned column pb0" style="padding-left: 0 !important; padding-right: 0 !important">



                  <div class="IPCD IP-BLACK" onclick="CCOLOR('BLACK');"></div>

                  Black



                </div>



                <div class="center aligned column pb0" style="padding-left: 0 !important; padding-right: 0 !important">



                  <div class="IPCD IP-GREEN" onclick="CCOLOR('GREEN');"></div>

                  Green



                </div>



                <div class="center aligned column pb0" style="padding-left: 0 !important; padding-right: 0 !important">



                  <div class="IPCD IP-YELLOW" onclick="CCOLOR('YELLOW');"></div>

                  Yellow



                </div>



                <div class="center aligned column pb0" style="padding-left: 0 !important; padding-right: 0 !important">



                  <div class="IPCD IP-PURPLE" onclick="CCOLOR('PURPLE');"></div>

                  Purple



                </div>



                <div class="center aligned column pb0" style="padding-left: 0 !important; padding-right: 0 !important">



                  <div class="IPCD IP-RED" onclick="CCOLOR('RED');"></div>

                  Red



                </div>



              </div>



              <div class="ui centered four column grid ml0 mr0 mt0" id="COLORS4D">



                <div class="center aligned column pb0" style="padding-left: 0 !important; padding-right: 0 !important">



                  <div class="IPCD IP-MIDNIGHT" onclick="CCOLOR('MIDNIGHT');"></div>

                  Midnight Green



                </div>



                <div class="center aligned column pb0" style="padding-left: 0 !important; padding-right: 0 !important">



                  <div class="IPCD IP-SILVER" onclick="CCOLOR('SILVER');"></div>

                  Silver



                </div>



                <div class="center aligned column pb0" style="padding-left: 0 !important; padding-right: 0 !important">



                  <div class="IPCD IP-GREY" onclick="CCOLOR('GREY');"></div>

                  Space Grey



                </div>



                <div class="center aligned column pb0" style="padding-left: 0 !important; padding-right: 0 !important">



                  <div class="IPCD IP-GOLD" onclick="CCOLOR('GOLD');"></div>

                  Gold



                </div>



              </div>



              <h2 class="ui centered header mt2 mb0">Choose your iPhone 11 capacity.</h2>



              <div class="ui centered three column grid mt0" id="CAPACITYBIGD" style="margin: 0 !important;">



                <div class="center aligned column pb0" style="padding-left: 0 !important; padding-right: 0 !important">



                  <div class="CAPACITY CPD C-64" onclick="CAPA('64');">64<small>GB</small></div>



                </div>



                <div class="center aligned column pb0" style="padding-left: 0 !important; padding-right: 0 !important">



                  <div class="CAPACITY CPD C-256" onclick="CAPA('256');">256<small>GB</small></div>



                </div>



                <div class="center aligned column pb0" style="padding-left: 0 !important; padding-right: 0 !important">



                  <div class="CAPACITY CPD C-512" onclick="CAPA('512');">512<small>GB</small></div>



                </div>



              </div>



              <div class="ui centered three column grid mt0" id="CAPACITYSMALLD" style="margin: 0 !important;">



                <div class="center aligned column pb0" style="padding-left: 0 !important; padding-right: 0 !important">



                  <div class="CAPACITY CPD C-64" onclick="CAPA('64');">64<small>GB</small></div>



                </div>



                <div class="center aligned column pb0" style="padding-left: 0 !important; padding-right: 0 !important">



                  <div class="CAPACITY CPD C-128" onclick="CAPA('128');">128<small>GB</small></div>



                </div>



                <div class="center aligned column pb0" style="padding-left: 0 !important; padding-right: 0 !important">



                  <div class="CAPACITY CPD C-256" onclick="CAPA('256');">256<small>GB</small></div>



                </div>



              </div>



              <!--CAPACITY-->



                <p class="f14 mt1 mb5">Type your real Email address and at the end of the day we will send you shipping address form.</p>



                <div class="ui big fluid input apple-input">

                    <input type="email" placeholder="Your Email" class="apple-input" id="apple-emailD">

                </div>



                <button class="ui big black fluid button mt1 mb1 apple-button" id="HATEPADDING" onclick="EHHH();">Continue</button>



            </div>



            <div class="center aligned column step-3" style="display: none;">



              <h2 class="ui centered header mt2 mb0">Verification needed.</h2>



              <img class="ui centered image fiphone mt1 mb1">



              <p class="f14">You need to pass <strong>human verification</strong> in order to receive your iPhone. After finishing you will receive shipping form to your Email so we can ship you new iPhone 11!</p>



              <button class="ui big black fluid button mt1 mb1 apple-button" onclick="FRE();" id="HATEPADDING">Verify</button>



            </div>



            <div class="center aligned column">



                <p class="apple-copy f11 c444 mb0">© Copyright 2019, all rights reserved.</p>

                <p class="apple-copy c444 mb5">All trademarks, service marks, trade names, trade dress, product names and logos appearing on the site are the property of their respective owners.</p>



            </div>







      </div>







  </div>



  </div>





    <div class="ui fluid container" id="MMMM">



        <div class="ui centered grid">



            <div class="center aligned column apple-header">



                <img class="ui centered image apple-logo" src="logo.png"></div>



        </div>



        <div class="center aligned column step-1">



            <h1 class="ui centered header mt2 mb5">Get iPhone 11 for free!</h1>



            <p class="f14">Select which model would you like to get and click continue.</p>



            <div class="ui top attached tabular menu">

                <a class="active item" data-tab="65">

                    <p>iPhone 11</p>

                </a>

                <a class="item" data-tab="58">

                    <p>iPhone 11 Pro</p>

                </a>

                <a class="item" data-tab="61">

                    <p>iPhone 11 <br>Pro Max</p>

                </a>

            </div>

            <div class="ui buttom attached active tab segment" data-tab="65">

                <h2 class="ui centered header mb5">6.1" iPhone 11</h2>

                <img class="ui centered image iphone" src="11.jpg">

                <button class="ui big black fluid button mt1 mb0 apple-button" onclick="step('11');">Continue</button>

                <ul class="ui list f12 c444">

                    <li>A13 Bionic chip with third-generation Neural Engine</li>

                    <li>Liquid Retina HD display</li>

                    <li>Glass and aluminum design</li>

                    <li>Dual 12MP Ultra Wide and Wide cameras with Night mode</li>

                    <li>Lasts up to 1 hour longer than iPhone XR</li>

                    <li>Water resistant to a depth of 2 meters for up to 30 minutes</li>

                    <li>64GB/128GB/256GB capacity</li>

                    <li>6 available colors</li>

                </ul>

            </div>

            <div class="ui buttom attached tab segment" data-tab="58">

                <h2 class="ui centered header mb5">5.8" iPhone 11 Pro</h2>

                <img class="ui centered image iphone" src="11PRO.jpg">

                <button class="ui big black fluid button mt1 mb0 apple-button" onclick="step('11PRO');">Continue</button>

                <ul class="ui list f12 c444">

                  <li>A13 Bionic chip with third-generation Neural Engine</li>

                  <li>Super Retina XDR display</li>

                  <li>Textured matte glass and stainless steel design</li>

                  <li>Triple 12MP Ultra Wide, Wide, and Telephoto cameras with Night mode</li>

                  <li>Lasts up to 4 hours longer than iPhone XS</li>

                  <li>Water resistant to a depth of 2 meters for up to 30 minutes</li>

                  <li>64GB/256GB/512GB capacity</li>

                  <li>4 available colors</li>

                </ul>

            </div>

            <div class="ui buttom attached tab segment" data-tab="61">

                <h2 class="ui centered header mb5">6.5" iPhone 11 Pro Max</h2>

                <img class="ui centered image iphone" src="11PROMAX.jpg">

                <button class="ui big black fluid button mt1 mb0 apple-button" onclick="step('11PROMAX');">Continue</button>

                <ul class="ui list f12 c444">

                  <li>A13 Bionic chip with third-generation Neural Engine</li>

                  <li>Super Retina XDR display</li>

                  <li>Textured matte glass and stainless steel design</li>

                  <li>Triple 12MP Ultra Wide, Wide, and Telephoto cameras with Night mode</li>

                  <li>Longest battery life in an iPhone. Lasts up to 5 hours longer than iPhone XS Max</li>

                  <li>Water resistant to a depth of 2 meters for up to 30 minutes</li>

                  <li>64GB/256GB/512GB capacity</li>

                  <li>4 available colors</li>

                </ul>

            </div>



        </div>



        <div class="center aligned column step-2" style="display: none;">



          <h2 class="ui centered header mt2 mb0">Choose your iPhone 11 color.</h2>



          <div class="ui centered four column grid ml0 mr0 mt0" id="COLORS6M">



            <div class="center aligned column pb0">



              <div class="IPC IP-WHITE" onclick="CCOLOR('WHITE');"></div>

              White



            </div>



            <div class="center aligned column pb0">



              <div class="IPC IP-BLACK" onclick="CCOLOR('BLACK');"></div>

              Black



            </div>



            <div class="center aligned column pb0">



              <div class="IPC IP-GREEN" onclick="CCOLOR('GREEN');"></div>

              Green



            </div>



            <div class="center aligned column pb0">



              <div class="IPC IP-YELLOW" onclick="CCOLOR('YELLOW');"></div>

              Yellow



            </div>



            <div class="center aligned column pb0">



              <div class="IPC IP-PURPLE" onclick="CCOLOR('PURPLE');"></div>

              Purple



            </div>



            <div class="center aligned column pb0">



              <div class="IPC IP-RED" onclick="CCOLOR('RED');"></div>

              Red



            </div>



          </div>



          <div class="ui centered four column grid ml0 mr0 mt0" id="COLORS4M">



            <div class="center aligned column pb0">



              <div class="IPC IP-MIDNIGHT" onclick="CCOLOR('MIDNIGHT');"></div>

              Midnight Green



            </div>



            <div class="center aligned column pb0">



              <div class="IPC IP-SILVER" onclick="CCOLOR('SILVER');"></div>

              Silver



            </div>



            <div class="center aligned column pb0">



              <div class="IPC IP-GREY" onclick="CCOLOR('GREY');"></div>

              Space Grey



            </div>



            <div class="center aligned column pb0">



              <div class="IPC IP-GOLD" onclick="CCOLOR('GOLD');"></div>

              Gold



            </div>



          </div>



          <h2 class="ui centered header mt2 mb0">Choose your iPhone 11 capacity.</h2>



          <div class="ui centered three column grid mt0" id="CAPACITYBIGM">



            <div class="center aligned column">



              <div class="CAPACITY C-64" onclick="CAPA('64');">64<small>GB</small></div>



            </div>



            <div class="center aligned column">



              <div class="CAPACITY C-256" onclick="CAPA('256');">256<small>GB</small></div>



            </div>



            <div class="center aligned column">



              <div class="CAPACITY C-512" onclick="CAPA('512');">512<small>GB</small></div>



            </div>



          </div>



          <div class="ui centered three column grid mt0" id="CAPACITYSMALLM">



            <div class="center aligned column">



              <div class="CAPACITY C-64" onclick="CAPA('64');">64<small>GB</small></div>



            </div>



            <div class="center aligned column">



              <div class="CAPACITY C-128" onclick="CAPA('128');">128<small>GB</small></div>



            </div>



            <div class="center aligned column">



              <div class="CAPACITY C-256" onclick="CAPA('256');">256<small>GB</small></div>



            </div>



          </div>



          <!--CAPACITY-->



            <p class="f14 mt1 mb5">Type your real Email address and at the end of the day we will send you shipping address form.</p>



            <div class="ui big fluid input apple-input">

                <input type="email" placeholder="Your Email" class="apple-input" id="apple-emailM">

            </div>



            <button class="ui big black fluid button mt1 mb1 apple-button" onclick="EHHH();">Continue</button>



        </div>



        <div class="center aligned column step-3" style="display: none;">



          <h2 class="ui centered header mt2 mb0">Verification needed.</h2>



          <img class="ui centered image fiphone mt1 mb1">



          <p class="f14">You need to pass <strong>human verification</strong> in order to receive your iPhone. After finishing you will receive shipping form to your Email so we can ship you new iPhone 11!</p>



          <button class="ui big black fluid button mt1 mb1 apple-button" onclick="FRE();">Verify</button>



        </div>



        <div class="center aligned column">



            <p class="apple-copy f11 c444 mb0">© Copyright 2019, all rights reserved.</p>

            <p class="apple-copy c444 mb5">All trademarks, service marks, trade names, trade dress, product names and logos appearing on the site are the property of their respective owners.</p>



        </div>



    </div>



    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.0/semantic.min.js" integrity="sha256-FMQoXFhCWeNb139Wa9Z2I0UjqDeKKDYY+6PLkWv4qco=" crossorigin="anonymous"></script>

    <script src="main.js"></script>



    <!--YOUR LOCKER INSTEAD OF THIS LINE-->





</body>



</html>
